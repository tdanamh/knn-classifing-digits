﻿import numpy as np
import matplotlib.pyplot as plt

# loading images and labels
train_images = np.loadtxt("data\\train_images.txt")
test_images = np.loadtxt("data\\test_images.txt")
train_labels = np.loadtxt("data\\train_labels.txt", 'int8')
test_labels = np.loadtxt("data\\test_labels.txt", 'int8')

#plot the first 100 training images with their labels in a 10 x 10 subplot
#print("\nThe first 100 labels for training images")
nbImages = 10

#set the number of pixels 500x500
plt.figure(figsize=(5,5))

for i in range(nbImages**2):
    plt.subplot(nbImages,nbImages,i+1)
    plt.axis('off')
    plt.imshow(np.reshape(train_images[i,:],(28,28)),cmap = "gray")
#plt.show()
labels_nbImages = train_labels[:nbImages**2]
#print(np.reshape(labels_nbImages,(nbImages,nbImages)))

#plot the first 100 testing images with their labels in a 10 x 10 subplot
#print("\nThe first 100 labels for testing images")

nbImages = 10
plt.figure(figsize=(5,5))
for i in range(nbImages**2):
    plt.subplot(nbImages,nbImages,i+1)
    plt.axis('off')
    plt.imshow(np.reshape(test_images[i,:],(28,28)),cmap = "gray")
#plt.show()
labels_nbImages = test_labels[:nbImages**2]
#print(np.reshape(labels_nbImages,(nbImages,nbImages)))

def classify_image(train_images, train_labels, test_image, k=3, d='l2', verbose=False):
    diferente = test_image - train_images
    if d == 'l2':  # dist euclidiana
        suma_distantelor = np.sqrt(np.sum(diferente ** 2, axis=1))
    elif d == 'l1':  # dist manhattan
        suma_distantelor = np.sum(np.abs(diferente), axis=1)
    else:
        print("Nu ati introdus un tip de distanta valida")
        return 1

    # in suma_distantelor avem un vector(1000, ) cu distantele intre imaginea de test si restul ex de antrenare
    # deoarece dorim distanta cea mai mica, le sortam indicii cu valori crescatoare
    indici = np.argsort(suma_distantelor)

    # cei mai apropiati k vecini(etichetele lor)
    vecini = train_labels[indici[0:k]]
    # print("Labels for the nearest neighbours of ", test_labels[0], "calculated with d = ", d, "are: ", vecini)

    # Care va fi eticheta asignata?(vecinul predominant)
    contor = np.bincount(vecini)  # vector de frecventa pt vecini
    etichetaPrezisa = contor.argmax()  # cel mai frecvent
    # print("Eticheta prezisa ", etichetaPrezisa)

    # plotare
    if verbose:
        plt.figure(figsize=(k, 1))
        for i in range(k):
            plt.subplot(1, k, i + 1)
            plt.axis('off')
            current_image = train_images[indici[i]]
            plt.imshow(np.reshape(current_image, (28, 28)), cmap="gray")
        plt.show()

    return etichetaPrezisa


# acuratetea = cate etichete prezise sunt egale cu ce e adevarat?
def acuratete(etichete_prezise, what_is_true):
    etichete_prezise = np.array(etichete_prezise)
    what_is_true = np.array(what_is_true)

    boolean_index = (etichete_prezise == what_is_true)  # vector cu true/false. Pastreaza doar True

    return len(what_is_true[boolean_index]) * 100 / len(what_is_true)


file = open("predictii_3nn_l2_mnist.txt", "w")
# file.write("Hello World")
# salvam etichetele prezise in fisier pt toate imaginile de test
for index in range(len(test_images)):
    testingImage = test_images[index, :]
    file.write(str(classify_image(train_images, train_labels, testingImage, 3, 'l2')))

file.close()
file = open("predictii_3nn_l2_mnist.txt", "r")
etichetePreziseFisier = []

for linie in file:
    for character in linie:
        etichetePreziseFisier.append(int(character))
file.close()
# print(etichetePreziseFisier)
print("Acuratetea 3NN l2 pe multimea de testare este ", acuratete(etichetePreziseFisier, test_labels))


# Pentru matricea C, fiecare element cij reprezintă nr exemplelor din clasa i care au fost clasificate ca fiind
# în clasa j.

def confusion_matrix(what_is_true, etichete_prezise):
    etichete_prezise = np.array(etichete_prezise)
    what_is_true = np.array(what_is_true)
    m = np.zeros((10, 10))  # sunt 10 clase
    for true, predictie in zip(what_is_true, etichete_prezise):
        m[true, predictie] += 1
    return m


matrice_confuzie = confusion_matrix(test_labels, etichetePreziseFisier)
print("Matricea de confuzie \n", matrice_confuzie)


# Perechea de cifre (5, 3) cea mai des confundată, având șase misclasificări. Afișați exemplele de cifra 5
# misclasificate precum și cei trei vecini. Mai jos este afișat primul din cele șase astfel de cazuri
print("Elem 5 este confundat de ", matrice_confuzie[5][3], "cu elementul 3")
# print(type(etichetePreziseFisier)) #class list

for index, (predictie, adevar) in enumerate(zip(etichetePreziseFisier, test_labels)):
    if adevar == 5 and predictie == 3:
        img_adevar = test_images[index, :]
        # plot img_adevar
        plt.figure(figsize=(1, 1))
        plt.axis('off')
        plt.imshow(np.reshape(img_adevar, (28, 28)), cmap="gray")
        plt.show()
        # plot cei 3 vecini pt img_adevar
        classify_image(train_images, train_labels, img_adevar, 3, 'l2', True)


# Calculați acuratețea metodei celor mai apropiați vecini pe mulțimea de testare având ca distanța l2 și numărul de
# vecini ∈ [1, 3, 5, 7, 9].
# a. Plotați un grafic cu acuratețea obținuta pentru fiecare vecin și salvați scorurile în fișierul acuratete_l2.txt.
# b. Repetați punctul anterior pentru distanța l1. Plotați graficul de la punctul anterior în aceeași figură cu
# graficul curent
# (utilizați fișierul acuratete_l1.txt)

from collections import defaultdict

dict_predictii = defaultdict(list)

for distanta in ['l1', 'l2']:
    for k in [1, 3, 5, 7, 9]:
        predictii_imagini_test = []
        for index_imagini_test in range(0, len(test_images)):
            img_test = test_images[index_imagini_test, :]
            predictie = classify_image(train_images, train_labels, img_test, k, distanta)
            predictii_imagini_test.append(predictie)
        cheie = 'k = ' + str(k) + ', distanta = ' + distanta
        dict_predictii[cheie] = predictii_imagini_test

# salvare acuratete pt l2 si l1 in fisiere
file1 = open("acuratete_l1.txt", "w")
file2 = open("acuratete_l2.txt", "w")

for cheie, lista_valori_prezise in dict_predictii.items():
    if 'l1' in cheie:
        file1.write(str(acuratete(lista_valori_prezise, test_labels)) + " ")
    elif 'l2' in cheie:
        file2.write(str(acuratete(lista_valori_prezise, test_labels)) + " ")
file1.close()
file2.close()

file1 = open("acuratete_l1.txt", "r")
file2 = open("acuratete_l2.txt", "r")

# plotare grafice cu vectorul de vecini si acuratetile din fisiere
nr_vecini = [1, 3, 5, 7, 9]
list_acuratete_l1 = []
list_acuratete_l2 = []

for line1_fisier1 in file1:
    list_acuratete_l1 = line1_fisier1.split()  # class list
for line1_fisier2 in file2:
    list_acuratete_l2 = line1_fisier2.split()  # class list

plt.plot(nr_vecini, list_acuratete_l1, label="distanta l1")
plt.plot(nr_vecini, list_acuratete_l2, label="distanta l2")

plt.xlabel('x - nr vecini')
plt.ylabel('y - acuratete')
plt.title('Analiza performantei clasificatorului knn pt l1, k [1,3,5,7,9]')
plt.legend()
plt.show()
